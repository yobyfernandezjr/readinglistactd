//Part 2:
/*
Create a simple server and the following routes with their corresponding HTTP methods and responses:
  If the url is http://localhost:8000/, send a response Welcome to Ordering System
  If the url is http://localhost:8000/dashboard, send a response Welcome to your User's Dashboard!
  If the url is http://localhost:8000/products, send a response Here’s our products available
  If the url is http://localhost:8000/addProduct, send a response Add a course to our resources
      - create a mock datebase of products that has these fields: (name, description, price, stocks)
      - use the request_body to add new products.
  If the url is http://localhost:8000/updateProduct, send a response Update a course to our resources
  If the url is http://localhost:8000/archiveProduct, send a response Archive courses to our resources
Test each endpoints in POSTMAN and save the screenshots

*/
let http = require ('http');
let port = 8000;

// Mock database
let products = [
  {
    "name": "productA",
    "description": "New product in Town",
    "price": "100 Php",
    "stocks": 10
  },
  {
    "name": "productB",
    "description": "Better than productA",
    "price": "150 Php",
    "stocks": 10
  },
];

let server = http.createServer(function(req, res){
  if(req.url == '/' && req.method == 'GET') {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Welcome to Ordering System');
  } else if (req.url == '/dashboard' && req.method == 'GET') {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end("Welcome to your User's Dashboard!");
  } else if (req.url == '/products' && req.method == 'GET') {
    res.writeHead(200, {'Content-Type': 'application/json'});
    res.end("Here's our products available");
  } else if (req.url == '/addproducts' && req.method == 'POST') {
      let request_body = '';
      req.on('data', function(data){
        request_body += data;
      });
      req.on('end', function(data){
        request_body = JSON.parse(request_body);
        let newProduct = {
          "name": request_body.name,
          "description": request_body.description,
          "price": request_body.price,
          "stocks": request_body.stocks
        }
        products.push(newProduct);
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.write(JSON.stringify(newProduct));
        res.end('Add a course to our resources'); 
      })  
  } else if (req.url == '/updateProduct' && req.method == 'PUT') {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end("Update a course to our resources");
  } else if (req.url == '/archiveProduct' && req.method == 'DELETE') {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end("Archive product to our resources");
  }
})
server.listen(port);
console.log(`Server is running at localhost: ${port}`);